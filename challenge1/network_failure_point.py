import networkx as nx
from collections import defaultdict

def get_max_value(data):
  d = defaultdict(list)
  for key, value in data.items():
    d[value].append(key)
  return max(d.items())[1]

def identify_router(G):
  degrees = {node:val for (node, val) in G.degree()}
  return get_max_value(degrees)

print(identify_router(nx.gnp_random_graph(10, 0.3, None, True)))
